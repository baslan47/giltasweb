<?php 
$footer=$db->prepare("SELECT * from settings");
$footer->execute();
$datas=$footer->fetchAll(PDO::FETCH_ASSOC);
foreach($datas as $m)
{
?>
<footer class="main-footer">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-7.png)"></div>
		<div class="pattern-layer-two" style="background-image: url(images/background/pattern-8.png)"></div>
		<!--Waves end-->
    	<div class="auto-container">
        	<!--Widgets Section-->
            <div class="widgets-section">
            	<div class="row clearfix">
                	
                    <!-- Column -->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
						<div class="row clearfix">
						
                        	<!-- Footer Column -->
                            <div class="footer-column col-lg-7 col-md-6 col-sm-12">
                                <div class="footer-widget logo-widget">
									<div class="logo">
										<a href="?do=mainpage"><img src="images/giltasheader.png" alt=""  style="height: 60px;width: 180px;"></a>
									</div>
									<div class="text">Müşterilerini, müşterileri için kaçınılmaz kılar</div>
									<!-- Social Box -->
									<ul class="social-box">
										<li><a href="<?php echo $m["facebookurl"] ?>" class="fa fa-facebook-f"></a></li>
										<li><a href="<?php echo $m["linkedinurl"] ?>" class="fa fa-linkedin"></a></li>
										<li><a href="<?php echo $m["instagramurl"] ?>" class="fa fa-instagram"></a></li>
									</ul>
								</div>
							</div>
							
							<!-- Footer Column -->
                            
							
						</div>
					</div>
					
					<!-- Column -->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
						<div class="row clearfix">
							
							<!-- Footer Column -->
							
							
							<!-- Footer Column -->
							<div class="footer-column col-lg-6 col-md-6 col-sm-12">
								<div class="footer-widget contact-widget">
									<h5>İletişim</h5>
									<ul>
										<li>
											<span class="icon flaticon-placeholder-2"></span>
											<strong>Adres</strong>
											Necatibey Bulvarı, Ege Yıldız İş Hanı 14/4 Çankaya-İZMİR
										</li>
										<li>
											<span class="icon flaticon-phone-call"></span>
											<strong>Telefon</strong>
											<a href="tel:+902324468252">0 232 446 82 52 (pbx)</a>
										</li>
										<li>
											<span class="icon flaticon-email-1"></span>
											<strong>E-Mail</strong>
											<a href="mailto:info@giltas.com.tr">info@giltas.com.tr</a>
										</li>
									</ul>
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
			
			<!-- Footer Bottom -->
			<div class="footer-bottom">
				<div class="auto-container">
					<div class="row clearfix">
						<!-- Column -->
						<div class="column col-lg-6 col-md-12 col-sm-12">
							<div class="copyright">Copyright &copy;  All Rights Reserved.</div>
						</div>
						<!-- Column -->
						
					</div>
				</div>
			</div>
			
		</div>
	</footer>

	<?php } ?>