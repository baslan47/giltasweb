    <?php $id=@$_GET["id"]; ?>
    <section class="page-title">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-16.png)"></div>
    	<div class="auto-container">
        <?php
                $serv=$db->prepare("select * from pages where id=$id");
                $serv->execute();
                $datas=$serv->fetchALL(PDO::FETCH_ASSOC);
                foreach($datas as $m)
                {
                ?>
			<h2><?php echo $m["baslik"] ?></h2>
			<?php } ?>
        </div>
    </section>

    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            <?php
                $serv=$db->prepare("select * from pages where id=$id");
                $serv->execute();
                $datas=$serv->fetchALL(PDO::FETCH_ASSOC);
                foreach($datas as $m)
                {
                ?>
				
				<!-- Content Side -->
                <div class="content-side right-sidebar col-lg-12 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							<h2><?php echo $m["baslik"] ?></h2>
							<div class="image">
								<img src="<?php echo $m["image"] ?>" alt="" />
							</div>
							<p><?php echo $m["text"]?></p>
							
						</div>
					</div>
				</div>
				
                <?php } ?>

			</div>
		</div>
	</div>
	<?php $set=$db->prepare("SELECT * FROM settings")
            ;$set->execute()
            ;$datas=$set->fetchALL(PDO::FETCH_ASSOC);
            foreach($datas as $m)
            { ?>

	<!-- Info Section -->
	<section class="info-section" style="background-image: url(images/giltasbg.png)">
		<div class="auto-container">
			<div class="row clearfix">
				
				<!-- Logo Column -->
				<div class="logo-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="logo">
							<a href="?do=mainpage"><img src="images/giltasheader.png" alt="" /></a>
						</div>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="icon-box"><span class="flaticon-pin"></span></div>
						<ul>
							<li><strong>Adres</strong></li>
							<li><?php echo $m["address"] ?></li>
						</ul>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="icon-box"><span class="flaticon-phone-call"></span></div>
						<ul>
							<li><strong>Telefon</strong></li>
							<li><?php echo $m["phone"] ?></li>
						</ul>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="icon-box"><span class="flaticon-email-1"></span></div>
						<ul>
							<li><strong>E-Mail</strong></li>
							<li><?php echo $m["email"] ?></li>
						</ul>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<?php } ?>