
<?php include("inc/ayar.php"); ?>
<header class="main-header header-style-one">
    	
		<!-- Header Top -->
        <div class="header-top">
            <div class="auto-container">
                <div class="clearfix">
					<!-- Top Left -->
					<div class="top-left">
						<!-- Info List -->
						<ul class="info-list">
							<li><a href="mailto:info@giltas.com.tr"><span class="icon flaticon-email"></span> info@giltas.com.tr</a></li>
							<li><a href="tel:786-875-864-75"><span class="icon flaticon-telephone"></span> 0 232 446 82 52 (pbx)</a></li>
						</ul>
					</div>
					
					<!-- Top Right -->
                    <div class="top-right pull-right">
						<!-- Social Box -->
						<?php
						$sm=$db->prepare("select * from settings");
						$sm->execute();
						$datas=$sm->fetchAll(PDO::FETCH_ASSOC);
						foreach($datas as  $m)
						{
						?>
						<ul class="social-box">
							<li><a href="<?php echo $m["facebookurl"] ?>" class="fa fa-facebook-f"></a></li>
							<li><a href="<?php echo $m["instagramurl"] ?>" class="fa fa-instagram"></a></li>
							<li><a href="<?php echo $m["linkedinurl"] ?>" class="fa fa-linkedin"></a></li>
							<li><div class="logo"><a href="https://www.nebim.com.tr/tr"><img src="uploads/nebim.png" alt="" title="" style="widht:50px;height:40px"></a></div></li>
						</ul>
						<?php } ?>
                    </div>
					
                </div>
            </div>
        </div>
		
		<!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container clearfix">
            	
				<div class="pull-left logo-box">
					<div class="logo"><a href="?do=mainpage"><img src="images/logo-giltas.jpg" alt="" title="" style="height: 60px;width: 180px;"></a></div>
				</div>
				
				<div class="nav-outer clearfix">
					<!--Mobile Navigation Toggler-->
					<div class="mobile-nav-toggler"><span class="icon flaticon-menu"></span></div>
					<!-- Main Menu -->
					<nav class="main-menu navbar-expand-md">
						<div class="navbar-header">
							<!-- Toggle Button -->    	
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						
						<div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
							<ul class="navigation clearfix">
								<li ><a href="?do=mainpage">Anasayfa</a>
									<!--<ul>
										<li class="dropdown"><a href="#">Home Pages</a>
											<ul>
												<li><a href="index.html">Home Page 01</a></li>
												<li><a href="index-2.html">Home Page 02</a></li>
												<li><a href="index-3.html">Home Page 03</a></li>
											</ul>
										</li>
										<li class="dropdown"><a href="#">Header styles</a>
											<ul>
												<li><a href="index.html">Header Style 01</a></li>
												<li><a href="index-2.html">Header Style 02</a></li>
												<li><a href="index-3.html">Header Style 03</a></li>
												<li><a href="index-4.html">Header Style 04</a></li>
												<li><a href="index-5.html">Header Style 05</a></li>
												<li><a href="index-6.html">Header Style 06</a></li>
											</ul>
										</li>
									</ul> -->
								</li>
								<li class="dropdown"><a href="#">Hakkımızda</a>
									<ul>
										<?php 
										$about=$db->prepare("SELECT * from about ");
										$about->execute();
										$datas=$about->fetchALL(PDO::FETCH_ASSOC);
										foreach($datas as $m)
										{ ?>
											<li><a href="?do=about&id=<?php echo $m["id"] ?>"><?php echo $m["name"] ?></a></li>
										<?php } ?>
										
										
									</ul>
								</li>
								<li class="dropdown"><a href="#">Hizmetlerimiz</a>
									<ul>
										<?php
										$hizmet=$db->prepare("select * from categories");
										$hizmet->execute();
										$datas=$hizmet->fetchALL(PDO::FETCH_ASSOC);
										foreach($datas as $m)
										{ ?>
											<li><a href="?do=hizmet&id=<?php echo $m["id"] ?>"><?php echo $m["catName"] ?></a></li>
										<?php }
										?>
										
										
									</ul>
								</li>
								<li><a href="?do=references">Referanslarımız</a>
									
								</li>

								<!--<li class="dropdown"><a href="#">Shop</a>
									<ul>
										<li><a href="shop.html">Our Products</a></li>
										<li><a href="shop-single.html">Product Single</a></li>
										<li><a href="shopping-cart.html">Shopping Cart</a></li>
										<li><a href="checkout.html">Checkout</a></li>
										<li><a href="account.html">Account</a></li>
									</ul>
								</li> -->
								<li class="dropdown"><a href="#">Blog</a>
									<ul>
										<?php
										$blog=$db->prepare("select * from blogcat ");
										$blog->execute();
										$datas = $blog->fetchALL(PDO::FETCH_ASSOC);
										foreach($datas as $m)
										{ ?>
											<li><a href="?do=blog&id=<?php echo $m["id"] ?>"><?php echo $m["blogCatName"] ?></a></li>

										<?php }
										?> 
										
										
									</ul>
								</li>
								<li><a href="?do=contact">İletişim</a></li>
								
							</ul>
						</div>
					</nav>
					
					<!-- Main Menu End-->
					<div class="outer-box clearfix">
					
						<!-- Cart Box 
						<div class="cart-box">
							<div class="dropdown">
								<button class="cart-box-btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="flaticon-shopping-bag-1"></span><span class="total-cart">2</span></button>
								<div class="dropdown-menu pull-right cart-panel" aria-labelledby="dropdownMenu1">

									<div class="cart-product">
										<div class="inner">
											<div class="cross-icon"><span class="icon fa fa-remove"></span></div>
											<div class="image"><img src="images/resource/post-thumb-1.jpg" alt="" /></div>
											<h3><a href="shop-single.html">Flying Ninja</a></h3>
											<div class="quantity-text">Quantity 1</div>
											<div class="price">$99.00</div>
										</div>
									</div>
									<div class="cart-product">
										<div class="inner">
											<div class="cross-icon"><span class="icon fa fa-remove"></span></div>
											<div class="image"><img src="images/resource/post-thumb-2.jpg" alt="" /></div>
											<h3><a href="shop-single.html">Patient Ninja</a></h3>
											<div class="quantity-text">Quantity 1</div>
											<div class="price">$99.00</div>
										</div>
									</div>
									<div class="cart-total">Sub Total: <span>$198</span></div>
									<ul class="btns-boxed">
										<li><a href="shoping-cart.html">View Cart</a></li>
										<li><a href="checkout.html">CheckOut</a></li>
									</ul>

								</div>
							</div>
						</div>
						-->
						<!-- Search Btn -->
						<div class="search-box-btn search-box-outer"><span class="icon fa fa-search"></span></div>
						
						<!-- Nav Btn -->
						<div class="nav-btn navSidebar-button"><span class="icon flaticon-menu-2"></span></div>
						
						<!-- Quote Btn 
						<div class="btn-box">
							<a href="contact.html" class="theme-btn btn-style-one"><span class="txt">Free Consulting</span></a>
						</div>
							-->
					</div>
				</div>
				
            </div>
        </div>
        <!--End Header Upper-->
        
		<!-- Sticky Header  -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.html" title=""><img src="images/logo-giltas.jpg" style="height: 60px;width: 180px;" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav><!-- Main Menu End-->
					
					<!-- Main Menu End-->
					<div class="outer-box clearfix">
					
						<!-- Cart Box 
						<div class="cart-box">
							<div class="dropdown">
								<button class="cart-box-btn dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="flaticon-shopping-bag-1"></span><span class="total-cart">2</span></button>
								<div class="dropdown-menu pull-right cart-panel" aria-labelledby="dropdownMenu">

									<div class="cart-product">
										<div class="inner">
											<div class="cross-icon"><span class="icon fa fa-remove"></span></div>
											<div class="image"><img src="images/resource/post-thumb-1.jpg" alt="" /></div>
											<h3><a href="shop-single.html">Flying Ninja</a></h3>
											<div class="quantity-text">Quantity 1</div>
											<div class="price">$99.00</div>
										</div>
									</div>
									<div class="cart-product">
										<div class="inner">
											<div class="cross-icon"><span class="icon fa fa-remove"></span></div>
											<div class="image"><img src="images/resource/post-thumb-2.jpg" alt="" /></div>
											<h3><a href="shop-single.html">Patient Ninja</a></h3>
											<div class="quantity-text">Quantity 1</div>
											<div class="price">$99.00</div>
										</div>
									</div>
									<div class="cart-total">Sub Total: <span>$198</span></div>
									<ul class="btns-boxed">
										<li><a href="shoping-cart.html">View Cart</a></li>
										<li><a href="checkout.html">CheckOut</a></li>
									</ul>

								</div>
							</div>
						</div>
						-->
						<!-- Search Btn -->
						<div class="search-box-btn search-box-outer"><span class="icon fa fa-search"></span></div>
						
						<!-- Nav Btn -->
						<div class="nav-btn navSidebar-button"><span class="icon flaticon-menu"></span></div>
						
					</div>
					
                </div>
            </div>
        </div><!-- End Sticky Menu -->
    
		<!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-multiply"></span></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.html"><img src="images/logo.png" alt="" title=""></a></div>
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            </nav>
        </div><!-- End Mobile Menu -->
	
    </header>
    <!-- End Main Header -->
	
	<!-- Sidebar Cart Item -->
	<div class="xs-sidebar-group info-group">
		<div class="xs-overlay xs-bg-black"></div>
		<div class="xs-sidebar-widget">
			<div class="sidebar-widget-container">
				<div class="widget-heading">
					<a href="#" class="close-side-widget">
						X
					</a>
				</div>
				<div class="sidebar-textwidget">
					
					<!-- Sidebar Info Content -->
					<div class="sidebar-info-contents">
						<div class="content-inner">
							<div class="logo">
								<a href="index.html"><img src="images/logo-giltas.jpg" alt="" style="height: 80px;width: 200px;"/></a>
							</div>
							<div class="content-box">
								<h2>Giltaş A.Ş.</h2>
								
								<a href="?do=contact" class="theme-btn btn-style-two"><span class="txt">Bize Ulaşın</span></a>
							</div>
							<?php
						$sm=$db->prepare("select * from settings");
						$sm->execute();
						$datas=$sm->fetchAll(PDO::FETCH_ASSOC);
						foreach($datas as  $m)
						{
						?>
							<div class="contact-info">
								<h2>İletişim Bilgileri</h2>
								<ul class="list-style-one">
									<li><span class="icon fa fa-location-arrow"></span><?php echo $m["address"] ?>/li>
									<li><span class="icon fa fa-phone"></span><?php echo $m["phone"] ?></li>
									<li><span class="icon fa fa-envelope"></span><?php echo $m["email"]?></li>
								
								</ul>
							</div>
							<!-- Social Box -->
							<ul class="social-box">
							<li><a href="<?php echo $m["facebookurl"] ?>" class="fa fa-facebook-f"></a></li>
							<li><a href="<?php echo $m["instagramurl"] ?>" class="fa fa-instagram"></a></li>
							<li><a href="<?php echo $m["linkedinurl"] ?>" class="fa fa-linkedin"></a></li>
							</ul>
							<?php } ?>


						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>