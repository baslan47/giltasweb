<?php
$id=@$_GET["id"];

?>
<section class="page-title">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-16.png)"></div>
    	<div class="auto-container">
        <?php
										$hizmet=$db->prepare("select * from categories where id=$id");
										$hizmet->execute();
										$datas=$hizmet->fetchALL(PDO::FETCH_ASSOC);
										foreach($datas as $m)
										{ ?>
											<h2><?php echo $m["catName"] ?></h2>
										<?php }
										?>
			
			
        </div>
    </section>

    	<!-- Services Page Section -->
	<section class="services-page-section">
		<div class="auto-container">
			<div class="row clearfix">
				<?php
                $serv=$db->prepare("select * from pages where catID=$id");
                $serv->execute();
                $datas=$serv->fetchALL(PDO::FETCH_ASSOC);
                foreach($datas as $m)
                {
                ?>
				<!-- News Block Three -->
				<div class="news-block-three col-lg-6 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<a href="?do=hizmet-detay&id=<?php echo $m["id"] ?>"><img src="<?php 
								if($m["image"]=="") 
								{ 
									echo "uploads/giltaslogo2.jpg" ;
									
									} 
									else 
									{ 
										echo $m["image"] ;
										}  ?>" alt="" style="width: 100%;height:100%"/></a>
						</div>
						<div class="lower-content">
							<div class="content">
								<div class="icon-box">
									<span class="icon flaticon-coding-1"></span>
								</div>
								<h4><a href="?do=hizmet-detay&id=<?php echo $m["id"] ?>"><?php echo $m["pageName"] ?></a></h4>
								
								<a class="read-more" href="?do=hizmet-detay&id=<?php echo $m["id"] ?>">Daha Fazlası<span class="arrow flaticon-long-arrow-pointing-to-the-right"></span></a>
							</div>
						</div>
					</div>
				</div>
				
				<?php } ?>
				
			</div>
		</div>
	</section>