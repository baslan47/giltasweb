
<!-- <div id="myModal" class="modal fade">
	<div class="modal-dialog modal-newsletter">
		<div class="modal-content">
			
				<div class="modal-header">
					
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span>&times;</span></button>
				</div>
				<a href="https://us06web.zoom.us/webinar/register/WN_du9RRBbGR0-ajJ7L2xfSZA"><img src="images/webinar-2.jpg"></a>
					
		</div>
	</div>
</div> -->
<section class="banner-section">

		<div class="main-slider-carousel owl-carousel owl-theme">
		<?php 
			$slide=$db->prepare("SELECT * FROM slides order by line ");
			$slide->execute();
			$datas=$slide->fetchALL(PDO::FETCH_ASSOC);
			foreach($datas as $m)
			{ ?>
            <div class="slide" style="background-image: url(<?php echo $m["image"] ?>); background-size:100%;height:100%">
				<div class="patern-layer-one" style="background-image: url(images/main-slider/pattern-1.png)"></div>
				<div class="patern-layer-two" style="background-image: url(images/main-slider/pattern-2.png)"></div>
				<div class="auto-container">
					
					<!-- Content Column -->
					<div class="content-column">
						<div class="inner-column">
							<div class="patern-layer-three" style="background-image: url(images/main-slider/pattern-3.png)"></div>
							<div class="title"> <?php echo $m["text1"] ?></div>
							<h1> <?php echo $m["text2"] ?> </h1>
							
							<div class="btns-box">
								<a href="<?php echo $m["url"] ?>" class="theme-btn btn-style-one"><span class="txt">Daha Fazlası İçin Tıklayınız</span></a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
			
			<?php } ?>
		</div>
		
	</section>
	<!-- End Banner Section -->
	
	<!-- About Section -->
	<section class="about-section">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title">
				
				<h2>Neden Nebim V3</h2>
			</div>
			<div class="row clearfix">
				
				<!-- Content Column -->
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column">
						<div class="text">Nebim V3 ERP sayesinde A'dan Z'ye tüm iş süreçlerinizi aynı platform üzerinde ve entegre bir şekilde yönetin</div>
						<div class="blocks-outer">
						
							<!-- Feature Block -->
							<div class="feature-block">
								<div class="inner-box">
									<div class="icon flaticon-award-1"></div>
									<h6>Kapsamlı ve Entegre</h6>
									<div class="feature-text">Satın almadan üretime, finans yönetiminden satışa kadar ihtiyaç duyduğunuz tüm iş süreçlerinizi, aynı platform üzerinde, baştan sona entegre bir şekilde yönetin</div>
								</div>
							</div>
							
							<!-- Feature Block -->
							<div class="feature-block">
								<div class="inner-box">
									<div class="icon flaticon-technical-support"></div>
									<h6>Değişime ve Büyümeye Açık</h6>
									<div class="feature-text">Modüler yapısı ve başka sistemlerle entegrasyon yetenekleri sayesinde ERP’nizi firmanızın büyümesini sağlayacak platform olarak kullanın</div>
								</div>
							</div>

							<div class="feature-block">
								<div class="inner-box">
									<div class="icon flaticon-technical-support"></div>
									<h6>Hızlı Uyarlanabilir</h6>
									<div class="feature-text">Sektörünüze özel hazır fonksiyonları ve Nebim’in geliştirdiği hızlı ERP uyarlama yöntemleri sayesinde yeni projelerinizi hızla ve çeviklikle hayata geçirin</div>
								</div>
							</div>
							
							
						</div>
						
					
						
					</div>
				</div>
				
				<!-- Images Column -->
				<div class="images-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column" style="background-image: url(images/icons/globe.png)">
						<div class="pattern-layer" style="background-image: url(images/background/pattern-1.png)"></div>
						<div class="images-outer parallax-scene-1">
							<div class="image" data-depth="0.10">
								<img src="images/resource/is-sureci.png" alt="" />
							</div>
							<div class="image-two" data-depth="0.30">
								<img src="images/resource/v3.png" alt="" />
							</div>
							<div class="image-three" data-depth="0.20">
								<img src="images/resource/giltas_innovative_logo.png" alt="" />
							</div>
							<div class="image-four" data-depth="0.30">
								<img src="images/resource/nebim-gold.png" alt="" />
							</div>
						</div>
					</div>
					
				</div>
				
			</div>
		</div>
	</section>
	<!-- End About Section -->
	<section class="testimonial-section">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title">
				<div class="clearfix">
					<div class="pull-left">
						<div class="title"> </div>
						<h2>Duyurular & Haberler</h2></h2>
					</div>
					<div class="pull-right">
						<div class="text">Duyuru ve Haberlerimizi Takipte Kalın <br>Son gelişmelerden ilk siz haberdar olun.</div>
					</div>
				</div>
			</div>
			<div class="testimonial-carousel owl-carousel owl-theme">
				

			<?php 
				$duyhab=$db->prepare("SELECT * FROM blog where blogCatID=3 order by id desc limit 8");
				$duyhab->execute();
				$datas=$duyhab->fetchALL(PDO::FETCH_ASSOC);
				foreach($datas as $m)
				{ ?>
				<div class="testimonial-block" onclick="location.href='?do=blog-detay&id=<?php echo $m['id'] ?>';">
					<div class="inner-box" style="background-image: url(images/background/pattern-4.png)">
						<div class="upper-box">
							<div class="icon">
								<img src="<?php 
								if($m["image"]=="") 
								{ 
									echo "uploads/giltaslogo.jpg" ;
									
									} 
									else 
									{ 
										echo $m["image"] ;
										}  ?>" alt="" style="width: 100px;height:80px;"/>
							</div>
							<h4> <?php echo $m["baslik"] ?> </h4>
							<div class="designation"><?php echo $m["date"] ?></div>
						</div>
						<div class="text"><?php echo substr($m["text"],0,200)  ?></div>
					</div>
				</div>
				
				<?php }
				?>


				<!-- Testimonial Block -->
				
				<!-- Testimonial Block -->
				
				
			</div>
			
			
			
		</div>
	</section>
	<!-- Featured Section -->
	<section class="featured-section">
		<div class="auto-container">
			<div class="row clearfix">
				
				<!-- Featured Block Two -->
				<div class="feature-block-two col-lg-6 col-md-12 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms" style="background-image: url(images/giltas2.jpg)">
						<div class="number">35 +</div>
						<h4>İş Ortağımızla</h4>
						<div class="text">Verimliliğinizi En Üst Düzeye Çıkarmaktayız<br>.</div>
					</div>
				</div>
				
				<!-- Featured Block Two -->
				<div class="feature-block-two col-lg-6 col-md-12 col-sm-12">
					<div class="inner-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms" style="background-image: url(images/giltas2.jpg)">
						<div class="number">1000 +</div>
						<h4>Referansımızla</h4>
						<div class="text">Tekstil ve Konfeksiyon,Ayakkabı, Halı - Çeyiz - Mobilya , Optik / Saat alanlarında çözüm üretmekteyiz.</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Featured Section -->
	
	<!-- Services Section -->
	<section class="services-section margin-top">
		<div class="pattern-layer" style="background-image: url(images/background/pattern-2.png)"></div>
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title light centered">
				<div class="title">  </div>
				<h2>Başarı Hikayeleri </h2>
			</div>
			<div class="row clearfix">
				

			<?php 
				$duyhab=$db->prepare("SELECT * FROM blog where blogCatID=2 order by id desc limit 4");
				$duyhab->execute();
				$datas=$duyhab->fetchALL(PDO::FETCH_ASSOC);
				foreach($datas as $m)
				{ ?>
				
				<div class="service-block col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="icon-box">
							<span class="icon flaticon-responsive"></span>
						</div>
						<h5><a href="?do=blog-detay&id=<?php echo $m["id"] ?>"><?php echo $m["baslik"] ?></a></h5>
						<div class="text"><?php echo substr($m["text"],0,99).'...' ;?></div>
						<a href="?do=blog-detay&id=<?php echo $m["id"] ?>" class="arrow flaticon-long-arrow-pointing-to-the-right"></a>
					</div>
				</div>
				
				<?php }
				?>
				<!-- Service Block -->
				
				
				<!-- Service Block -->
				
				
			</div>
		</div>
	</section>
	<!-- End Services Section -->
	
	<!-- Services Section Two -->
	<section class="services-section-two margin-top">
		<div class="auto-container">
			<div class="upper-box">
				<div class="icon-one" style="background-image: url(images/icons/icon-1.png)"></div>
				<div class="icon-two" style="background-image: url(images/icons/icon-2.png)"></div>
				<div class="icon-three" style="background-image: url(images/icons/icon-3.png)"></div>
				<!-- Sec Title -->
				<div class="sec-title light centered">
					<div class="title"> </div>
					<h2>Çözümlerimiz</h2>
				</div>
			</div>
			<div class="inner-container">
				<div class="row clearfix">
					
					<!-- Service Block Two -->
					<div class="service-block-two col-lg-4 col-md-6 col-sm-12">
						<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="shape-one"></div>
							<div class="shape-two"></div>
							<div class="icon-box">
								<span class="icon flaticon-coding-1"></span>
							</div>
							<h5><a href="?do=hizmet-detay&id=35">Bulut Çözümleri</a></h5>
							<div class="text">   </div>
						</div>
					</div>
					
					<!-- Service Block Two -->
					<div class="service-block-two col-lg-4 col-md-6 col-sm-12">
						<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="shape-one"></div>
							<div class="shape-two"></div>
							<div class="icon-box">
								<span class="icon flaticon-mobile-app"></span>
							</div>
							<h5><a href="?do=hizmet-detay&id=36">Mobil Çözümler</a></h5>
							<div class="text">  </div>
						</div>
					</div>
					
					<!-- Service Block Two -->
					<div class="service-block-two col-lg-4 col-md-6 col-sm-12">
						<div class="inner-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="shape-one"></div>
							<div class="shape-two"></div>
							<div class="icon-box">
								<span class="icon flaticon-computer"></span>
							</div>
							<h5><a href="?do=hizmet-detay&id=37">Yapay Zeka</a></h5>
							<div class="text">   </div>
						</div>
					</div>
					
					<!-- Service Block Two -->
					<div class="service-block-two col-lg-4 col-md-6 col-sm-12">
						<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="shape-one"></div>
							<div class="shape-two"></div>
							<div class="icon-box">
								<span class="icon flaticon-web"></span>
							</div>
							<h5><a href="?do=hizmet-detay&id=34">KVKK</a></h5>
							<div class="text">  </div>
						</div>
					</div>
					
					<!-- Service Block Two -->
					<div class="service-block-two col-lg-4 col-md-6 col-sm-12">
						<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="shape-one"></div>
							<div class="shape-two"></div>
							<div class="icon-box">
								<span class="icon flaticon-monitor-2"></span>
							</div>
							<h5><a href="?do=hizmet-detay&id=38">Endüstri 4.0</a></h5>
							<div class="text">  </div>
						</div>
					</div>
					
					<!-- Service Block Two -->
					<div class="service-block-two col-lg-4 col-md-6 col-sm-12">
						<div class="inner-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="shape-one"></div>
							<div class="shape-two"></div>
							<div class="icon-box">
								<span class="icon flaticon-human-resources"></span>
							</div>
							<h5><a href="?do=hizmet-detay&id=39">E-Dönüşüm</a></h5>
							<div class="text">  </div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>
	<!-- End Services Section Two -->
	
	<!-- Call To Action Section -->
	<section class="call-to-action-section" style="background-image: url(images/background/pattern-3.png)">
		<div class="auto-container">
			<div class="row clearfix">
				<!-- Heading Column -->
				<div class="heading-column col-lg-8 col-md-12 col-sm-12">
					<div class="inner-column">
						<h2>Hemen Demo Talep Formunu Doldurun <br> Size Ulaşalım </h2>
					</div>
				</div>
				<!-- Button Column -->
				<div class="button-column col-lg-4 col-md-12 col-sm-12">
					<div class="inner-column">
						<a href="?do=demo" class="theme-btn btn-style-two"><span class="txt">Tıklayın</span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Call To Action Section -->
	
	<!-- Cases Section 
	<section class="cases-section">
		<div class="auto-container">
			<!-- Sec Title 
			<div class="sec-title centered">
				<div class="title">LATEST CASE STUDIES</div>
				<h2>Reads Now Our Recent <br> Projects Studies</h2>
			</div>
			<div class="row clearfix">
				
				<!-- Case Block 
				<div class="case-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<img src="images/gallery/1.jpg" alt="" />
							<div class="overlay-box">
								<a href="images/gallery/1.jpg" data-fancybox="case" data-caption="" class="search-icon"><span class="icon flaticon-search"></span></a>
								<div class="content">
									<h4><a href="projects-detail.html">Social Media App</a></h4>
									<div class="category">Ideas / Technology</div>
								</div>
								<a href="projects-detail.html" class="arrow flaticon-long-arrow-pointing-to-the-right"></a>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Case Block 
				<div class="case-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<img src="images/gallery/2.jpg" alt="" />
							<div class="overlay-box">
								<a href="images/gallery/2.jpg" data-fancybox="case" data-caption="" class="search-icon"><span class="icon flaticon-search"></span></a>
								<div class="content">
									<h4><a href="projects-detail.html">Social Media App</a></h4>
									<div class="category">Ideas / Technology</div>
								</div>
								<a href="projects-detail.html" class="arrow flaticon-long-arrow-pointing-to-the-right"></a>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Case Block 
				<div class="case-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<img src="images/gallery/3.jpg" alt="" />
							<div class="overlay-box">
								<a href="images/gallery/3.jpg" data-fancybox="case" data-caption="" class="search-icon"><span class="icon flaticon-search"></span></a>
								<div class="content">
									<h4><a href="projects-detail.html">Social Media App</a></h4>
									<div class="category">Ideas / Technology</div>
								</div>
								<a href="projects-detail.html" class="arrow flaticon-long-arrow-pointing-to-the-right"></a>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Case Block 
				<div class="case-block col-lg-6 col-md-12 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<img src="images/gallery/4.jpg" alt="" />
							<div class="overlay-box">
								<a href="images/gallery/4.jpg" data-fancybox="case" data-caption="" class="search-icon"><span class="icon flaticon-search"></span></a>
								<div class="content">
									<h4><a href="projects-detail.html">Social Media App</a></h4>
									<div class="category">Ideas / Technology</div>
								</div>
								<a href="projects-detail.html" class="arrow flaticon-long-arrow-pointing-to-the-right"></a>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Case Block 
				<div class="case-block col-lg-6 col-md-12 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<img src="images/gallery/5.jpg" alt="" />
							<div class="overlay-box">
								<a href="images/gallery/5.jpg" data-fancybox="case" data-caption="" class="search-icon"><span class="icon flaticon-search"></span></a>
								<div class="content">
									<h4><a href="projects-detail.html">Social Media App</a></h4>
									<div class="category">Ideas / Technology</div>
								</div>
								<a href="projects-detail.html" class="arrow flaticon-long-arrow-pointing-to-the-right"></a>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
			<div class="section-text">We Have Done More Than 1K Projects in Last 3 Years, With 100% Satisfaction.</div>
			
			<div class="btn-box text-center">
				<a href="#" class="theme-btn btn-style-three"><span class="txt">View All</span></a>
			</div>
			
		</div>
	</section>
	<!-- End Cases Section -->
	
	<!--Sponsors Section
	<section class="sponsors-section">
		<div class="auto-container">
			
			<div class="carousel-outer">
               
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <li><div class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/2.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/3.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/4.png" alt=""></a></div></li>
					<li><div class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/2.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/3.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/4.png" alt=""></a></div></li>
                </ul>
            </div>
			
		</div>
	</section>-->
	<!--End Sponsors Section-->
	
	<!-- Testimonial Section -->
	
	<!-- End Testimonial Section -->
	
	<!-- Technology Section 
	<section class="technology-section" style="background-image: url(images/background/1.jpg)">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-5.png)"></div>
		<div class="pattern-layer-two" style="background-image: url(images/background/pattern-6.png)"></div>
		<div class="auto-container">
			<div class="row clearfix">
				<!-- Title Column 
				<div class="title-column col-lg-5 col-md-12 col-sm-12">
					<div class="inner-column">
						<!-- Sec Title 
						<div class="sec-title light">
							<div class="title">TECHNOLOGY INDEX</div>
							<h2>We Deliver Solutions with the Goal of Trusting Workships</h2>
						</div>
					</div>
				</div>
				<!-- Blocks Column 
				<div class="blocks-column col-lg-7 col-md-12 col-sm-12">
					<div class="inner-column">
						<div class="row clearfix">
							
							<!-- Technology Block 
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-coding-2"></span>
									</div>
									<h6>WEB</h6>
								</div>
							</div>
							
							<!-- Technology Block 
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-android"></span>
									</div>
									<h6>ANDROID</h6>
								</div>
							</div>
							
							<!-- Technology Block 
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-apple"></span>
									</div>
									<h6>IOS</h6>
								</div>
							</div>
							
							<!-- Technology Block
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-iot"></span>
									</div>
									<h6>IOT</h6>
								</div>
							</div>
							
							<!-- Technology Block
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-smartband"></span>
									</div>
									<h6>WEARALABLES</h6>
								</div>
							</div>
							
							<!-- Technology Block 
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-tv"></span>
									</div>
									<h6>TV</h6>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Technology Section -->
	
	<!-- Team Section
	<section class="team-section" style="background-image: url(images/background/2.jpg)">
		<div class="auto-container">
			<!-- Sec Title
			<div class="sec-title">
				<div class="clearfix">
					<div class="pull-left">
						<div class="title">OUR DEDICATED TEAM</div>
						<h2>We have Large No <br> of Expert Team Member</h2>
					</div>
					<div class="pull-right">
						<div class="text">Our goal is to help our companies maintain or achieve best- in-class <br> positions in their respective industries and our team works.</div>
					</div>
				</div>
			</div>
			<div class="clearfix">
				
				<!-- Team Block 
				<div class="team-block col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<a href="team.html"><img src="images/resource/team-1.jpg" alt="" /></a>
						</div>
						<div class="lower-box">
							<!-- Social Box 
							<ul class="social-box">
								<li><a href="#" class="fa fa-facebook-f"></a></li>
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-dribbble"></a></li>
							</ul>
							<div class="content">
								<h5><a href="team.html">Jennifer Garcia</a></h5>
								<div class="designation">Consultant Officer</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Team Block 
				<div class="team-block col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<a href="team.html"><img src="images/resource/team-2.jpg" alt="" /></a>
						</div>
						<div class="lower-box">
							<!-- Social Box 
							<ul class="social-box">
								<li><a href="#" class="fa fa-facebook-f"></a></li>
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-dribbble"></a></li>
							</ul>
							<div class="content">
								<h5><a href="team.html">Robert Liam</a></h5>
								<div class="designation">Web Designer</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Team Block 
				<div class="team-block col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<a href="team.html"><img src="images/resource/team-3.jpg" alt="" /></a>
						</div>
						<div class="lower-box">
							<!-- Social Box 
							<ul class="social-box">
								<li><a href="#" class="fa fa-facebook-f"></a></li>
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-dribbble"></a></li>
							</ul>
							<div class="content">
								<h5><a href="team.html">Jassica Ethan</a></h5>
								<div class="designation">Project Manager</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Team Block 
				<div class="team-block col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<a href="team.html"><img src="images/resource/team-4.jpg" alt="" /></a>
						</div>
						<div class="lower-box">
							<!-- Social Box 
							<ul class="social-box">
								<li><a href="#" class="fa fa-facebook-f"></a></li>
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-dribbble"></a></li>
							</ul>
							<div class="content">
								<h5><a href="team.html">Adaim Mrala</a></h5>
								<div class="designation">IT Service Officer</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Team Section -->
	
	<!-- News Section -->
	<section class="news-section">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title centered">
				<div class="title"> </div>
				<h2>Blog</h2>
			</div>
			<div class="row clearfix">
				<?php 
				$blog=$db->prepare("select * from blog where blogCatID=1 order by id desc limit 3");
				$blog->execute();
				$datas=$blog->fetchALL(PDO::FETCH_ASSOC);
				foreach($datas as $m)
				{ ?>
					<div class="news-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							<a href="?do=blog-detay&id=<?php echo $m["id"] ?>"><img src="<?php echo $m["image"] ?>" alt=""  style="width:370px;height:270px"></a>
						</div>
						<div class="lower-content">
							<div class="post-date"><span><?php echo $m["date"] ?></span></div>
							
							<h4><a href="?do=blog-detay&id=<?php echo $m["id"] ?>"><?php echo $m["baslik"] ?></a></h4>
							<div class="text"><?php echo substr($m["text"],0,70) ?></div>
							<a class="read-more" href="?do=blog-detay&id=<?php echo $m["id"] ?>">Daha Fazlası ... <span class="arrow flaticon-long-arrow-pointing-to-the-right"></span></a>
						</div>
					</div>
				</div>
			<?php 	} ?>
				<!-- News Block -->
				
				
				<!-- News Block -->
				
				
			</div>
		</div>
	</section>