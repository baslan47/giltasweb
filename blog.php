<?php $id=@$_GET["id"]; ?>
<section class="page-title">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-16.png)"></div>
    	<div class="auto-container">
        <?php
										$blog=$db->prepare("select * from blogcat where id=$id");
										$blog->execute();
										$datas = $blog->fetchALL(PDO::FETCH_ASSOC);
										foreach($datas as $m)
										{ ?>
											<h2><?php echo $m["blogCatName"] ?></h2>

										<?php }
										?> 

			
			
        </div>
    </section>
    <!--End Page Title-->
	
	<!-- Blog Page Section -->
    <section class="blog-page-section">
        <div class="auto-container">
			<div class="row clearfix">
				
                <?php 
                $blogs=$db->prepare("select * from blog where blogCatID=$id order by id desc");
                $blogs->execute();
                $datas=$blogs->fetchAll(PDO::FETCH_ASSOC);
                foreach($datas as $m)
                { ?>
                <div class="news-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							<a href="?do=blog-detay&id=<?php echo $m["id"] ?>"><img src="<?php 
								if($m["image"]=="") 
								{ 
									echo "uploads/giltaslogo2.jpg" ;
									
									} 
									else 
									{ 
										echo $m["image"] ;
										}  ?>" alt="" style="width: 370px;height:270px;"/></a>
						</div>
						<div class="lower-content">
							<div class="post-date"><span><?php echo $m["date"] ?></span></div>

							<h4><a href="?do=blog-detay&id=<?php echo $m["id"] ?>"><?php echo $m["baslik"] ?></a></h4>
							<div class="text"><?php echo substr($m["text"],0,200)  ?></div>
							<a class="read-more" href="?do=blog-detay&id=<?php echo $m["id"] ?>">Daha Fazlası<span class="arrow flaticon-long-arrow-pointing-to-the-right"></span></a>
						</div>
					</div>
				</div>
              <?php  }
                ?>

				<!-- News Block -->
				
				
				
			
		</div>
	</section>