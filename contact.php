    <section class="page-title">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-16.png)"></div>
    	<div class="auto-container">
			<h2>Bize Ulaşın</h2>
			
        </div>
    </section>

    <section class="contact-info-section">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="title-box" style="margin-bottom:50px">
				<div class="title">İletişim</div>
				<h2>Ürünlerimiz ve Fırsatlarımızla ilgili<br> Bilgi almak için bize ulaşabilirsiniz</h2>
				
			</div>
			
			<div class="row clearfix">
			
            <?php $set=$db->prepare("SELECT * FROM settings")
            ;$set->execute()
            ;$datas=$set->fetchALL(PDO::FETCH_ASSOC);
            foreach($datas as $m)
            { ?>

            
				<!-- Info Column -->
				<div class="info-column col-lg-4 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="content">
							<div class="icon-box"><span class="flaticon-pin"></span></div>
							<ul>
								<li><strong>Adres</strong></li>
								<li><?php echo $m["address"] ?></li>
							</ul>
						</div>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-4 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="content">
							<div class="icon-box"><span class="flaticon-phone-call"></span></div>
							<ul>
								<li><strong>Telefon</strong></li>
								<li><?php echo $m["phone"] ?></li>
                                <li><strong>FAX</strong></li>
                                <li><?php echo $m["fax"] ?></li>
							</ul>
						</div>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-4 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="content">
							<div class="icon-box"><span class="flaticon-email-1"></span></div>
							<ul>
								<li><strong>E-Mail</strong></li>
								<li><?php echo $m["email"] ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</section>

    <section class="contact-map-section">
		<div class="auto-container">
			<!-- Map Boxed -->
			<div class="map-boxed">
				<!--Map Outer-->
				<div class="map-outer">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3125.733819590068!2d27.133167315336458!3d38.42451587964641!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14bbd8fa4c002613%3A0x578b1232fb53585b!2zR2lsdGHFnyBTaXN0ZW0gQsO8dMO8bmxlxZ90aXJpY2k!5e0!3m2!1str!2str!4v1631165910563!5m2!1str!2str" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
				</div>
			</div>
		</div>
	</section>

    