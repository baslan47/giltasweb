<?php
$catid=@$_GET["id"];
?>
<?php
            $about=$db->prepare("select * from aboutpages where catID=$catid");
            $about->execute();
            $datas=$about->fetchAll(PDO::FETCH_ASSOC);
            foreach($datas as $m)
            {
            ?>
<section class="page-title">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-16.png)"></div>
    	<div class="auto-container">
            
			<h2><?php echo $m["pageName"] ?></h2>
			
           
        </div>
    </section>
    <!--End Page Title-->
	
	<!-- About Section -->
	<section class="about-section">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title">
				
				<h2><?php echo $m["baslik"] ?></h2>
			</div>
			<div class="row clearfix">
				
				<!-- Content Column -->
				<div class="content-column col-lg-12 col-md-12 col-sm-12">
					<div class="inner-column">
						<div class="text"><?php echo $m["text"] ?></div>
						<div class="blocks-outer">
						
							
							
						</div>
						
											
					</div>
				</div>
				
				<!-- Images Column -->
				
				
			</div>
		</div>
	</section>

    <?php } ?>
	<?php $set=$db->prepare("SELECT * FROM settings")
            ;$set->execute()
            ;$datas=$set->fetchALL(PDO::FETCH_ASSOC);
            foreach($datas as $m)
            { ?>

	<!-- Info Section -->
	<section class="info-section" style="background-image: url(images/giltasbg.png)">
		<div class="auto-container">
			<div class="row clearfix">
				
				<!-- Logo Column -->
				<div class="logo-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="logo">
							<a href="index.html"><img src="images/giltasheader.png" alt="" /></a>
						</div>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="icon-box"><span class="flaticon-pin"></span></div>
						<ul>
							<li><strong>Adres</strong></li>
							<li><?php echo $m["address"] ?></li>
						</ul>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="icon-box"><span class="flaticon-phone-call"></span></div>
						<ul>
							<li><strong>Telefon</strong></li>
							<li><?php echo $m["phone"] ?></li>
						</ul>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="icon-box"><span class="flaticon-email-1"></span></div>
						<ul>
							<li><strong>E-Mail</strong></li>
							<li><?php echo $m["email"] ?></li>
						</ul>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<?php } ?>