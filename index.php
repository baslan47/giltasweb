<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Giltaş Genel Pazarlama ve Ticaret A.Ş.</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Nunito+Sans:wght@300;600;700;800;900&display=swap" rel="stylesheet">

<!-- Color Switcher Mockup -->
<link href="css/color-switcher-design.css" rel="stylesheet">

<!-- Color Themes -->
<link id="theme-color-file" href="css/color-themes/default-theme.css" rel="stylesheet">
<meta name="description" content="Giltaş Müşterilerini, Müşterileri İçin Kaçınılmaz Kılar.">
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- React'i yükle. -->
  <!-- Not: yayınlama için hazırlanırken,  "development.js" yi "production.min.js" ile değiştirin -->
  <script src="https://unpkg.com/react@17/umd/react.development.js" crossorigin></script>
  <script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js" crossorigin></script>
  <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <style>
body {
	font-family: 'Varela Round', sans-serif;
}	
.modal-newsletter {	
	color: #999;
	font-size: 15px;
  	min-width: 600px;
}
.modal-newsletter .modal-content {
	padding: 40px;
	border-radius: 0;		
	border: none;
}
.modal-newsletter .modal-header {
	border-bottom: none;   
	position: relative;
	text-align: center;
	border-radius: 5px 5px 0 0;
}
.modal-newsletter h4 {
	color: #000;
	text-align: center;
	font-size: 30px;
	margin: 0 0 25px;
	font-weight: bold;
	text-transform: capitalize;
}
.modal-newsletter .close {
	background: #c0c3c8;
    position: absolute;
    top: 0;
    right: 0;
    color: #fff;
    text-shadow: none;
    opacity: 0.5;
    width: 30px;
    height: 30px;
    border-radius: 20px;
    font-size: 19px;
    text-align: center;
    padding: 0;
}
.modal-newsletter .close span {
	position: relative;
	top: -1px;
}
.modal-newsletter .close:hover {
	opacity: 0.8;
}
.modal-newsletter .icon-box {
	color: #7265ea;		
	display: inline-block;
	z-index: 9;
	text-align: center;
	position: relative;
	margin-bottom: 10px;
}
.modal-newsletter .icon-box i {
	font-size: 110px;
}
.modal-newsletter .form-control, .modal-newsletter .btn {
	min-height: 46px;
	border-radius: 3px; 
}
.modal-newsletter .form-control {
	box-shadow: none;
	border-color: #dbdbdb;
}
.modal-newsletter .form-control:focus {
	border-color: #7265ea;
	box-shadow: 0 0 8px rgba(114, 101, 234, 0.5);
}
.modal-newsletter .btn {
	color: #fff;
	border-radius: 4px;
	background: #7265ea;
	text-decoration: none;
	transition: all 0.4s;
	line-height: normal;
	padding: 6px 20px;
	min-width: 150px;
	border: none;
}
.modal-newsletter .btn:hover, .modal-newsletter .btn:focus {
	background: #4e3de4;
	outline: none;
}
.modal-newsletter .input-group {
	margin: 30px 0 15px;
}
.hint-text {
	margin: 100px auto;
	text-align: center;
}
</style>
<script>
$(document).ready(function(){
	$("#myModal").modal('show');
});
</script>
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body class="hidden-bar-wrapper">

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
    
 	<!-- Main Header-->
    <?php include("header.php") ?>
	<!-- END sidebar widget item -->
	
	<!-- Banner Section -->
    <?php
		$do=@$_GET["do"];
		switch($do)
		{
			case "mainpage":
				include("mainpage.php");
				break;
                
			case "demo":
                include("demo.php");
                break;

			case "blogs" :
				include("blogs.php");
				break;

            case "contact":
                include("contact.php");
                break;

            case "about":
                include("about.php");
                break;
            
            case "hizmet":
                include("services.php");
                break;
            
            case "hizmet-detay":
                include("services-detail.php");
                break;

            case "blog":
                include("blog.php");
                break;

            case "references":
                include("references.php");
                break;
            
            case "blog-detay":
                include("blog-detay.php");
                break;
            default:
				include("mainpage.php");
				break;
		}

	?>
	<!-- End News Section -->
	
	<!-- Main Footer -->
    <?php include("footer.php"); ?>
	
	
</div>
<!--End pagewrapper-->

<!-- Color Palate / Color Switcher -->

<div class="color-palate">
    <div class="color-trigger">
        <i class="fa fa-gear"></i>
    </div>
    <div class="color-palate-head">
        <h6>Choose Your Color</h6>
    </div>
	
	<div class="various-color clearfix">
        <div class="colors-list">
            <span class="palate default-color active" data-theme-file="css/color-themes/default-theme.css"></span>
            <span class="palate green-color" data-theme-file="css/color-themes/green-theme.css"></span>
            <span class="palate olive-color" data-theme-file="css/color-themes/olive-theme.css"></span>
            <span class="palate orange-color" data-theme-file="css/color-themes/orange-theme.css"></span>
            <span class="palate purple-color" data-theme-file="css/color-themes/purple-theme.css"></span>
            <span class="palate teal-color" data-theme-file="css/color-themes/teal-theme.css"></span>
            <span class="palate brown-color" data-theme-file="css/color-themes/brown-theme.css"></span>
            <span class="palate redd-color" data-theme-file="css/color-themes/redd-color.css"></span>
        </div>
    </div>
	
	<ul class="rtl-version option-box"> <li class="rtl">RTL Version</li> <li>LTR Version</li> </ul>
	
    <a href="#" class="purchase-btn">Purchase now $17</a>
    
    <div class="palate-foo">
        <span>You will find much more options for colors and styling in admin panel. This color picker is used only for demonstation purposes.</span>
    </div>

</div>

<!-- Search Popup -->
<div class="search-popup">
	<button class="close-search style-two"><span class="flaticon-multiply"></span></button>
	<button class="close-search"><span class="flaticon-up-arrow-1"></span></button>
	<form method="post" action="blog.html">
		<div class="form-group">
			<input type="search" name="search-field" value="" placeholder="Search Here" required="">
			<button type="submit"><i class="fa fa-search"></i></button>
		</div>
	</form>
</div>
<!-- End Header Search -->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>
<div class="scroll-to-top-right search-box-btn search-box-outer" data-target="html"><br><span class="icon fa fa-search marg-bot" style="font-size:22px;"></span></div>
<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/appear.js"></script>
<script src="js/parallax.min.js"></script>
<script src="js/tilt.jquery.min.js"></script>
<script src="js/jquery.paroller.min.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/nav-tool.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/script.js"></script>
<script src="js/color-settings.js"></script>

</body>
</html>